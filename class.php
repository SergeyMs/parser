<?php

require_once("vendor/autoload.php");

class HolidayParser
{

	protected $arResult;
	protected $dom;


	function getHtml($link)
	{
		$html = file_get_contents($link);
		$dom = phpQuery::newDocument($html);
		return $dom;
	}

	function getHolidays()
	{

		$holidays = $this->dom->find("ul.holidays-items a");
		foreach($holidays as $holiday)
		{
			$holiday = pq($holiday);
			$link = $holiday->attr("href");
			$obHolidayDetail = $this->getHtml($link);
			$holidayDetail = $obHolidayDetail->find(".holidays-text")->html();
			$this->arResult["HOLIDAYS"][] = array(
				"url" => $link,
				"name" => $holiday->text(),
				"detail" => $holidayDetail,
			);
		}
	}

	function getNameDay()
	{
		$nameday = $this->dom->find(".holidays-day p span");
		foreach($nameday as $name)
		{
			$name = pq($name);
			$this->arResult["NAMEDAY"][] = trim($name->text(), ", .");
		}
	}

	function getDailyEvents()
	{
		$events = $this->dom->find(".holidays-day-info-events p")->html();
		$arEvent = explode("<br>", $events);
		foreach($arEvent as $event)
		{
			preg_match("/^[0-9]+/", $event, $matches);
			$year = $matches[0];
			$this->arResult["DAILY_EVENTS"][] = array(
				"event_text" => preg_replace("/^[0-9]+\s[–-]\s/ui", "", $event),
				"year" => $year,
			);
		}
		
	}

	function setBornDied($object, $arrayName)
	{
		foreach($object as $human)
		{
			$human = pq($human);
			$imageLink = $human->find("img")->attr("src");
			$name = $human->find("b")->text();
			$yearStr = $human->find(".overflow-hidden")->find("div")->text();
			$year = preg_replace("/[^0-9]+/", "", $yearStr);
			$profile = $human->find("p")->text();
			$this->arResult[$arrayName][] = array(
				"name" => $name,
				"image" => $imageLink,
				"year" => $year,
				"profile" => $profile,
			);
		}
	}


	function getDailyBorn()
	{
		$obBorn = $this->dom->find(".holidays-day-info-events")->next()->next()->find("li");
		$this->setBornDied($obBorn, "DAILY_BORN");
	}

	function getDailyDied()
	{
		$obDied = $this->dom->find(".holidays-day-info-events")->next()->next()->next()->next()->find("li");
		$this->setBornDied($obDied, "DAILY_DIED");		
	}


	function init()
	{
		$this->dom = $this->getHtml("https://my-calend.ru/holidays/belarus");
		$this->getHolidays();
		$this->getNameDay();
		$this->getDailyEvents();
		$this->getDailyBorn();
		$this->getDailyDied();	
		phpQuery::unloadDocuments();
	}

	function getArray()
	{
		return $this->arResult;
	}


}


?>